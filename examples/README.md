# ARM LLVM 工具链在STM32上的应用教程

### 准备工作:
+ 下载[Windows Build Tools](https://github.com/xpack-dev-tools/windows-build-tools-xpack/releases/download/v4.4.0-1/xpack-windows-build-tools-4.4.0-1-win32-x64.zip)，解压并把bin文件夹添加到PATH
+ 安装[CMake](https://github.com/Kitware/CMake/releases/download/v3.26.3/cmake-3.26.3-windows-x86_64.msi)和[Ninja](https://github.com/ninja-build/ninja/releases/download/v1.11.1/ninja-win.zip)
+ 不会编写Makefile或者CMakeList.txt的，可以下载[CMakeList.txt模板](https://www.baidu.com)
