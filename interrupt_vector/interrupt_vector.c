#include <string.h>
#include <stdint.h>
#include <sys/cdefs.h>

/* 此文件中断向量表只适用于LLVM工具链 GCC请勿使用 */
#define Mapping(Address, Function)  [(Address)/4] = (void(*)(void))Function

void Default_Handler(void);
extern void _start(void);
extern uint8_t __stack[];

void NMI_Handler()                 __attribute__ ((weak, alias ("Default_Handler")));
void HardFault_Handler()           __attribute__ ((weak, alias ("Default_Handler")));
void MemManage_Handler()           __attribute__ ((weak, alias ("Default_Handler")));
void BusFault_Handler()            __attribute__ ((weak, alias ("Default_Handler")));
void UsageFault_Handler()          __attribute__ ((weak, alias ("Default_Handler")));
void SVC_Handler()                 __attribute__ ((weak, alias ("Default_Handler")));
void DebugMon_Handler()            __attribute__ ((weak, alias ("Default_Handler")));
void PendSV_Handler()              __attribute__ ((weak, alias ("Default_Handler")));
void SysTick_Handler()             __attribute__ ((weak, alias ("Default_Handler")));
void WWDG_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void PVD_IRQHandler()              __attribute__ ((weak, alias ("Default_Handler")));
void TAMPER_IRQHandler()           __attribute__ ((weak, alias ("Default_Handler")));
void RTC_IRQHandler()              __attribute__ ((weak, alias ("Default_Handler")));
void FLASH_IRQHandler()            __attribute__ ((weak, alias ("Default_Handler")));
void RCC_IRQHandler()              __attribute__ ((weak, alias ("Default_Handler")));
void EXTI0_IRQHandler()            __attribute__ ((weak, alias ("Default_Handler")));
void EXTI1_IRQHandler()            __attribute__ ((weak, alias ("Default_Handler")));
void EXTI2_IRQHandler()            __attribute__ ((weak, alias ("Default_Handler")));
void EXTI3_IRQHandler()            __attribute__ ((weak, alias ("Default_Handler")));
void EXTI4_IRQHandler()            __attribute__ ((weak, alias ("Default_Handler")));
void DMA1_Channel1_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void DMA1_Channel2_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void DMA1_Channel3_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void DMA1_Channel4_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void DMA1_Channel5_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void DMA1_Channel6_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void DMA1_Channel7_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void ADC1_2_IRQHandler()           __attribute__ ((weak, alias ("Default_Handler")));
void USB_HP_CAN1_TX_IRQHandler()   __attribute__ ((weak, alias ("Default_Handler")));
void USB_LP_CAN1_RX0_IRQHandler()  __attribute__ ((weak, alias ("Default_Handler")));
void CAN1_RX1_IRQHandler()         __attribute__ ((weak, alias ("Default_Handler")));
void CAN1_SCE_IRQHandler()         __attribute__ ((weak, alias ("Default_Handler")));
void EXTI9_5_IRQHandler()          __attribute__ ((weak, alias ("Default_Handler")));
void TIM1_BRK_IRQHandler()         __attribute__ ((weak, alias ("Default_Handler")));
void TIM1_UP_IRQHandler()          __attribute__ ((weak, alias ("Default_Handler")));
void TIM1_TRG_COM_IRQHandler()     __attribute__ ((weak, alias ("Default_Handler")));
void TIM1_CC_IRQHandler()          __attribute__ ((weak, alias ("Default_Handler")));
void TIM2_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void TIM3_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void TIM4_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void I2C1_EV_IRQHandler()          __attribute__ ((weak, alias ("Default_Handler")));
void I2C1_ER_IRQHandler()          __attribute__ ((weak, alias ("Default_Handler")));
void I2C2_EV_IRQHandler()          __attribute__ ((weak, alias ("Default_Handler")));
void I2C2_ER_IRQHandler()          __attribute__ ((weak, alias ("Default_Handler")));
void SPI1_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void SPI2_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void USART1_IRQHandler()           __attribute__ ((weak, alias ("Default_Handler")));
void USART2_IRQHandler()           __attribute__ ((weak, alias ("Default_Handler")));
void USART3_IRQHandler()           __attribute__ ((weak, alias ("Default_Handler")));
void EXTI15_10_IRQHandler()        __attribute__ ((weak, alias ("Default_Handler")));
void RTC_Alarm_IRQHandler()        __attribute__ ((weak, alias ("Default_Handler")));
void USBWakeUp_IRQHandler()        __attribute__ ((weak, alias ("Default_Handler")));
void TIM8_BRK_IRQHandler()         __attribute__ ((weak, alias ("Default_Handler")));
void TIM8_UP_IRQHandler()          __attribute__ ((weak, alias ("Default_Handler")));
void TIM8_TRG_COM_IRQHandler()     __attribute__ ((weak, alias ("Default_Handler")));
void TIM8_CC_IRQHandler()          __attribute__ ((weak, alias ("Default_Handler")));
void ADC3_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void FSMC_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void SDIO_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void TIM5_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void SPI3_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void UART4_IRQHandler()            __attribute__ ((weak, alias ("Default_Handler")));
void UART5_IRQHandler()            __attribute__ ((weak, alias ("Default_Handler")));
void TIM6_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void TIM7_IRQHandler()             __attribute__ ((weak, alias ("Default_Handler")));
void DMA2_Channel1_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void DMA2_Channel2_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void DMA2_Channel3_IRQHandler()    __attribute__ ((weak, alias ("Default_Handler")));
void DMA2_Channel4_5_IRQHandler()  __attribute__ ((weak, alias ("Default_Handler")));


__section(".data.init.enter") void (* const __Isr_Vector[])(void) __attribute((aligned(0x80))) = {
    [0] = (void(*)(void))__stack,
    [1] = (void(*)(void))_start,
    Mapping(0x008,	NMI_Handler),
    Mapping(0x00C,	HardFault_Handler),
    Mapping(0x010,	MemManage_Handler),
    Mapping(0x014,	BusFault_Handler),
    Mapping(0x018,	UsageFault_Handler),
    Mapping(0x01C,	NULL),
    Mapping(0x020,	NULL),
    Mapping(0x024,	NULL),
    Mapping(0x028,	NULL),
    Mapping(0x02C,	SVC_Handler),
    Mapping(0x030,	DebugMon_Handler),
    Mapping(0x034,	NULL),
    Mapping(0x038,	PendSV_Handler),
    Mapping(0x03C,	SysTick_Handler),
    Mapping(0x040,	WWDG_IRQHandler),
    Mapping(0x044,	PVD_IRQHandler),
    Mapping(0x048,	TAMPER_IRQHandler),
    Mapping(0x04C,	RTC_IRQHandler),
    Mapping(0x050,	FLASH_IRQHandler),
    Mapping(0x054,	RCC_IRQHandler),
    Mapping(0x058,	EXTI0_IRQHandler),
    Mapping(0x05C,	EXTI1_IRQHandler),
    Mapping(0x060,	EXTI2_IRQHandler),
    Mapping(0x064,	EXTI3_IRQHandler),
    Mapping(0x068,	EXTI4_IRQHandler),
    Mapping(0x06C,	DMA1_Channel1_IRQHandler),
    Mapping(0x070,	DMA1_Channel2_IRQHandler),
    Mapping(0x074,	DMA1_Channel3_IRQHandler),
    Mapping(0x078,	DMA1_Channel4_IRQHandler),
    Mapping(0x07C,	DMA1_Channel5_IRQHandler),
    Mapping(0x080,	DMA1_Channel6_IRQHandler),
    Mapping(0x084,	DMA1_Channel7_IRQHandler),
    Mapping(0x088,	ADC1_2_IRQHandler),
    Mapping(0x08C,	USB_HP_CAN1_TX_IRQHandler),
    Mapping(0x090,	USB_LP_CAN1_RX0_IRQHandler),
    Mapping(0x094,	CAN1_RX1_IRQHandler),
    Mapping(0x098,	CAN1_SCE_IRQHandler),
    Mapping(0x09C,	EXTI9_5_IRQHandler),
    Mapping(0x0A0,	TIM1_BRK_IRQHandler),
    Mapping(0x0A4,	TIM1_UP_IRQHandler),
    Mapping(0x0A8,	TIM1_TRG_COM_IRQHandler),
    Mapping(0x0AC,	TIM1_CC_IRQHandler),
    Mapping(0x0B0,	TIM2_IRQHandler),
    Mapping(0x0B4,	TIM3_IRQHandler),
    Mapping(0x0B8,	TIM4_IRQHandler),
    Mapping(0x0BC,	I2C1_EV_IRQHandler),
    Mapping(0x0C0,	I2C1_ER_IRQHandler),
    Mapping(0x0C4,	I2C2_EV_IRQHandler),
    Mapping(0x0C8,	I2C2_ER_IRQHandler),
    Mapping(0x0CC,	SPI1_IRQHandler),
    Mapping(0x0D0,	SPI2_IRQHandler),
    Mapping(0x0D4,	USART1_IRQHandler),
    Mapping(0x0D8,	USART2_IRQHandler),
    Mapping(0x0DC,	USART3_IRQHandler),
    Mapping(0x0E0,	EXTI15_10_IRQHandler),
    Mapping(0x0E4,	RTC_Alarm_IRQHandler),
    Mapping(0x0E8,	USBWakeUp_IRQHandler),
    Mapping(0x0EC,	TIM8_BRK_IRQHandler),
    Mapping(0x0F0,	TIM8_UP_IRQHandler),
    Mapping(0x0F4,	TIM8_TRG_COM_IRQHandler),
    Mapping(0x0F8,	TIM8_CC_IRQHandler),
    Mapping(0x0FC,	ADC3_IRQHandler),
    Mapping(0x100,	FSMC_IRQHandler),
    Mapping(0x104,	SDIO_IRQHandler),
    Mapping(0x108,	TIM5_IRQHandler),
    Mapping(0x10C,	SPI3_IRQHandler),
    Mapping(0x110,	UART4_IRQHandler),
    Mapping(0x114,	UART5_IRQHandler),
    Mapping(0x118,	TIM6_IRQHandler),
    Mapping(0x11C,	TIM7_IRQHandler),
    Mapping(0x120,	DMA2_Channel1_IRQHandler),
    Mapping(0x124,	DMA2_Channel2_IRQHandler),
    Mapping(0x128,	DMA2_Channel3_IRQHandler),
    Mapping(0x12C,	DMA2_Channel4_5_IRQHandler),
    Mapping(0x130,	NULL),
    Mapping(0x134,	NULL),
    Mapping(0x138,	NULL),
    Mapping(0x13C,	NULL),
    Mapping(0x140,	NULL),
    Mapping(0x144,	NULL),
    Mapping(0x148,	NULL),
    Mapping(0x14C,	NULL),
    Mapping(0x150,	NULL),
    Mapping(0x154,	NULL),
    Mapping(0x158,	NULL),
    Mapping(0x15C,	NULL),
    Mapping(0x160,	NULL),
    Mapping(0x164,	NULL),
    Mapping(0x168,	NULL),
    Mapping(0x16C,	NULL),
    Mapping(0x170,	NULL),
    Mapping(0x174,	NULL),
    Mapping(0x178,	NULL),
    Mapping(0x17C,	NULL),
    Mapping(0x180,	NULL),
    Mapping(0x184,	NULL),
    Mapping(0x188,	NULL),
    Mapping(0x18C,	NULL),
    Mapping(0x190,	NULL),
    Mapping(0x194,	NULL),
    Mapping(0x198,	NULL),
    Mapping(0x19C,	NULL),
    Mapping(0x1A0,	NULL),
    Mapping(0x1A4,	NULL),
    Mapping(0x1A8,	NULL),
    Mapping(0x1AC,	NULL),
    Mapping(0x1B0,	NULL),
    Mapping(0x1B4,	NULL),
    Mapping(0x1B8,	NULL),
    Mapping(0x1BC,	NULL),
    Mapping(0x1C0,	NULL),
    Mapping(0x1C4,	NULL),
    Mapping(0x1C8,	NULL),
    Mapping(0x1CC,	NULL),
    Mapping(0x1D0,	NULL),
    Mapping(0x1D4,	NULL),
    Mapping(0x1D8,	NULL),
    Mapping(0x1DC,	NULL),
    Mapping(0x1E0,	(void *)0xF1E0F85F),
};

__strong_reference(__Isr_Vector, __Interrupt_Vector);

void Default_Handler(void)
{
  for (;;);
}